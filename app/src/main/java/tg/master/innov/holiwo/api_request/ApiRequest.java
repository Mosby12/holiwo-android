package tg.master.innov.holiwo.api_request;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tg.master.innov.holiwo.utils.Utils;

public class ApiRequest {

    private static final String BASE_URL = Utils.getServerName();
    private static ApiRequest mInstance;
    private Retrofit retrofit;

    private ApiRequest() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized ApiRequest getInstance() {
        if (mInstance == null) {
            mInstance = new ApiRequest();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }
}
