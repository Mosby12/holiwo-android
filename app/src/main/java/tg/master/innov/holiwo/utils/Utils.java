package tg.master.innov.holiwo.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.regex.Pattern;

import tg.master.innov.holiwo.crypte_script.AESalgorithm;

public class Utils {

    public static final String PREF_USER_FIRST_TIME = "false";
    public static final String PREFERENCES_FILE = "User_config_files";
    private static final String SERVER_NAME = "http://10.0.2.2:3030/api/";
    private static final Pattern REGREX_MAIL = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
    private static final Pattern REGREX_PASSWORD = Pattern.compile("^(?=.*\\d).{4,8}$");

    static AESalgorithm aes = new AESalgorithm();

    public static String getServerName() {
        return SERVER_NAME;
    }

    public static Pattern getRegrexMail() {
        return REGREX_MAIL;
    }

    public static Pattern getRegrexPassword() {
        return REGREX_PASSWORD;
    }

    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {

        try {
            SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(settingName, aes.encrypt(settingValue));
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        /* return sharedPref.getString(settingName, defaultValue);*/
        String a = sharedPref.getString(settingName, defaultValue);
        String b = null;
        try {
            b = aes.decrypt(a);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b;
    }

    public void saveSharedFirst(Context ctx, String settingName, String settingValue) {

        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();

    }

    public String readSharedFirst(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }


}
