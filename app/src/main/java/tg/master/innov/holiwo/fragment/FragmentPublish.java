package tg.master.innov.holiwo.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.adapter.PublishRecyAdapter;
import tg.master.innov.holiwo.api_request.ApiRequest;
import tg.master.innov.holiwo.model.PublishModel;
import tg.master.innov.holiwo.utils.EndlessRecyclerViewScrollListener;
import tg.master.innov.holiwo.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPublish extends Fragment {

    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<PublishModel> modelPublishArrayList = new ArrayList<>();
    private PublishRecyAdapter adapterPublish;
    private PublishModel modelFeed;
    private SwipeRefreshLayout swipeContainer;
    private LinearLayout error404;
    SearchView searchView;
    private ProgressBar pg;
    // Store a member variable for the listener
    private EndlessRecyclerViewScrollListener scrollListener;



    public FragmentPublish() {
        // Required empty public constructor
    }

    public FragmentPublish(Context context) {
        this.context = context;
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {


            /* Toast.makeText(context, "Works!", Toast.LENGTH_LONG).show();*/
            // To keep animation for 3 seconds

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Stop animation (This will be after 3 seconds)
                    clear();
                    getAllPublicationConnect(1);
                    swipeContainer.setRefreshing(false);
                }
            }, 3000); // Delay in millis

        }
    };

    public static String getCurrentDate() {

        Calendar cal = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(cal.getTime());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_publish, container, false);

        init();
        pg.setVisibility(View.VISIBLE);
        clear();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterPublish);
        search();
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadNextDataFromApi(page);
            }
        };


        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);


        return view;
    }

    private void search() {

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                clear();
                getAllPublicationConnect(1);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                search_publication(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                clear();
                search_publication(newText);
                return false;
            }
        });
    }

    private void loadNextDataFromApi(int offset) {
        // Send an API request to retrieve appropriate paginated data
        //  --> Send the request including an offset value (i.e `page`) as a query parameter.
        //  --> Deserialize and construct new model objects from the API response
        //  --> Append the new data objects to the existing set of items inside the array of items
        //  --> Notify the adapter of the new items made with `notifyItemRangeInserted()`
        System.out.println(offset + "la page");
        getAllPublicationConnect(offset + 1);
    }

    // Append the next page of data into the adapter
    // This method probably sends out a network request and appends new data items to your adapter.

    private void otherMethode() {
        // 1. First, clear the array of data
        modelPublishArrayList.clear();
        // 2. Notify the adapter of the update
        adapterPublish.notifyDataSetChanged(); // or notifyItemRangeRemoved
        // 3. Reset endless scroll listener when performing a new search
        //scrollListener.resetState();
    }

    private void getAllPublicationConnect(int offset) {
        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .getPublication(offset);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        error404.setVisibility(View.GONE);
                        //get the response of the request
                        String stringRes = response.body().string();

                        JSONArray jsonArray = new JSONArray(stringRes);
//
                        /*Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_LONG).show();*/
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            int AdminId = jsonObject.getInt("AdminId");
                            String Categorie = jsonObject.getString("Categorie");
                            int CategorieId = jsonObject.getInt("CategorieId");
                            String Currency = jsonObject.getString("Currency");
                            int CurrencyId = jsonObject.getInt("CurrencyId");
                            URL attachment = new URL(Utils.getServerName() + jsonObject.getString("attachment"));
                            URL attachment1 = new URL(Utils.getServerName() + jsonObject.getString("attachment1"));
                            URL attachment2 = new URL(Utils.getServerName() + jsonObject.getString("attachment2"));
                            String Admin = jsonObject.getString("Admin");
                            String createdAt = jsonObject.getString("createdAt");
                            String updatedAt = jsonObject.getString("createdAt");
                            float price = jsonObject.getLong("price");
                            String content = jsonObject.getString("content");
                            int rate = jsonObject.getInt("rate");
                            String title = jsonObject.getString("title");
                            int likes = jsonObject.getInt("likes");
                            int state = jsonObject.getInt("state");

                            JSONObject symbol = new JSONObject(Currency);
                            JSONObject username = new JSONObject(Admin);
                            JSONObject propic = new JSONObject(Admin);
                            JSONObject categorieName = new JSONObject(Categorie);
                            URL propics = new URL(Utils.getServerName() + propic.getString("propic"));
                            String symbols = symbol.getString("symbol");
                            String usernames = username.getString("username");
                            String categorieNames = categorieName.getString("categorieName");

                            modelFeed = new PublishModel(id, likes, AdminId, CategorieId, CurrencyId, state, usernames, categorieNames, getDatefromJson(createdAt), content, title, createdAt, updatedAt, symbols, price, rate, propics, attachment, attachment1, attachment2);
                            swipeContainer.setRefreshing(false);
                            modelPublishArrayList.add(modelFeed);
                            adapterPublish.notifyDataSetChanged();
                            pg.setVisibility(View.GONE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //        swipeContainer.setRefreshing(false);
                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    // if a failure occurs
                    error404.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);
                    pg.setVisibility(View.GONE);
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            swipeContainer.setRefreshing(false);
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public void search_publication(String word) {
        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .search_publication(word);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        error404.setVisibility(View.GONE);
                        //get the response of the request
                        String stringRes = response.body().string();

                        JSONArray jsonArray = new JSONArray(stringRes);
//
                        /*Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_LONG).show();*/
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            int AdminId = jsonObject.getInt("AdminId");
                            String Categorie = jsonObject.getString("Categorie");
                            int CategorieId = jsonObject.getInt("CategorieId");
                            String Currency = jsonObject.getString("Currency");
                            int CurrencyId = jsonObject.getInt("CurrencyId");
                            URL attachment = new URL(Utils.getServerName() + jsonObject.getString("attachment"));
                            URL attachment1 = new URL(Utils.getServerName() + jsonObject.getString("attachment1"));
                            URL attachment2 = new URL(Utils.getServerName() + jsonObject.getString("attachment2"));
                            String Admin = jsonObject.getString("Admin");
                            String createdAt = jsonObject.getString("createdAt");
                            String updatedAt = jsonObject.getString("createdAt");
                            float price = jsonObject.getLong("price");
                            String content = jsonObject.getString("content");
                            int rate = jsonObject.getInt("rate");
                            String title = jsonObject.getString("title");
                            int likes = jsonObject.getInt("likes");
                            int state = jsonObject.getInt("state");

                            JSONObject symbol = new JSONObject(Currency);
                            JSONObject username = new JSONObject(Admin);
                            JSONObject propic = new JSONObject(Admin);
                            JSONObject categorieName = new JSONObject(Categorie);
                            URL propics = new URL(Utils.getServerName() + propic.getString("propic"));
                            String symbols = symbol.getString("symbol");
                            String usernames = username.getString("username");
                            String categorieNames = categorieName.getString("categorieName");
                            modelFeed = new PublishModel(id, likes, AdminId, CategorieId, CurrencyId, state, usernames, categorieNames, getDatefromJson(createdAt), content, title, createdAt, updatedAt, symbols, price, rate, propics, attachment, attachment1, attachment2);
                            swipeContainer.setRefreshing(false);
                            modelPublishArrayList.add(modelFeed);
                            adapterPublish.notifyDataSetChanged();
                            pg.setVisibility(View.GONE);

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        //        swipeContainer.setRefreshing(false);
                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    // if a failure occurs
                    error404.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);
                    pg.setVisibility(View.GONE);
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            swipeContainer.setRefreshing(false);
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void init() {
        pg = view.findViewById(R.id.init_progress_bar);
        error404 = view.findViewById(R.id.error404);
        error404.setVisibility(View.GONE);
        searchView = view.findViewById(R.id.search_publication);
        recyclerView = view.findViewById(R.id.publishrecyclerViewid);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        adapterPublish = new PublishRecyAdapter(context, modelPublishArrayList);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(onRefreshListener);
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        getAllPublicationConnect(1);
    }

    public class QueryEvent {
        String query;

        public QueryEvent(String query) {
            this.query = query;
        }

        public String getQuery() {
            return query;
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String getDatefromJson(String jsonDate) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(jsonDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String output = new SimpleDateFormat("dd/MM/yyyy").format(date);

        return output;
    }


    // Clean all elements of the recycler

    public void clear() {
        modelPublishArrayList.clear();
        adapterPublish.notifyDataSetChanged();
    }

}