package tg.master.innov.holiwo.api_request;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Api {

    @FormUrlEncoded
    @POST("users/login")
    Call<ResponseBody> login(
            @Field("mail") String mail,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("users/register")
    Call<ResponseBody> register(
            @Field("mail") String mail,
            @Field("username") String username,
            @Field("password") String password,
            @Field("name") String name,
            @Field("firstName") String firstName,
            @Field("address") String address,
            @Field("tel") String tel,
            @Field("propic") String propic,
            @Field("town") String town
    );

    @FormUrlEncoded
    @POST("publication/search")
    Call<ResponseBody> search_publication(
            @Field("searchWord") String searchWord
    );

    @GET("publication/{page}")
    Call<ResponseBody> getPublication(
            @Path("page") int page);


    @GET("publication/{publishId}/action")
    Call<ResponseBody> getlikeStateAction(
            @Header("Authorization") String token,
            @Path("publishId") int publishId);

    @POST("publication/{publishId}/vote/dislike")
    Call<ResponseBody> dislikeAction(
            @Header("Authorization") String token,
            @Path("publishId") int publishId);

    @POST("publication/{publishId}/vote/like")
    Call<ResponseBody> likeAction(
            @Header("Authorization") String token,
            @Path("publishId") int publishId);

    @FormUrlEncoded
    @PUT("users/profile/update/state/")
    Call<ResponseBody> deleteAccompte(
            @Header("Authorization") String token,
            @Field("state") int state);
}