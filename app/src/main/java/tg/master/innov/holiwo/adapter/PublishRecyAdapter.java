package tg.master.innov.holiwo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.activity.ChatActivity;
import tg.master.innov.holiwo.api_request.ApiRequest;
import tg.master.innov.holiwo.model.PublishModel;
import tg.master.innov.holiwo.utils.Utils;

public class PublishRecyAdapter extends RecyclerView.Adapter<PublishRecyAdapter.MyViewHolder> {

    protected Context context;
    private ArrayList<PublishModel> modelFeedArrayList;
    private RequestManager glide;
    Utils utils = new Utils();

    public PublishRecyAdapter(Context context, ArrayList<PublishModel> modelFeedArrayList) {

        this.context = context;
        this.modelFeedArrayList = modelFeedArrayList;
        glide = Glide.with(context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed, parent, false);

        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final PublishModel modelFeed = modelFeedArrayList.get(position);
        holder.tv_name.setText(modelFeed.getName());
        holder.tv_time.setText(modelFeed.getTime());
        holder.tv_likes.setText(String.valueOf(modelFeed.getLikes()));
        holder.tv_rate.setNumStars((int) modelFeed.getRate());
        holder.tv_context.setText(" " + modelFeed.getContent() + "\n Price:" + modelFeed.getPrice() + modelFeed.getSymbol() + "\n Categorie:" + modelFeed.getCategorieName());

        URL default_url = null;


        try {

            glide.load(modelFeed.getPropic()).centerCrop().placeholder(R.drawable.profile).error(R.drawable.profile).into(holder.imgView_proPic);
                glide.load(modelFeed.getAttachment()).into(holder.imgView_postPic);

        } catch (Exception e) {
            glide.load(R.drawable.profile).into(holder.imgView_proPic);
        }

        //requete start
        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .getlikeStateAction("Bearer " + utils.readSharedSetting(context, "token", "token"), modelFeed.getId());

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {

                        //get the response of the request
                        String stringRes = response.body().string();
                        if (response.code() == 201) {
                            holder.like_img.setImageResource(R.drawable.ic_stars_black_24dp);
                            holder.tv_like_info.setText("Like");
                            holder.likeState = false;
                        } else {
                            JSONObject res = new JSONObject(stringRes);
                            if (res.getInt("isLike") == 1) {
                                holder.like_img.setImageResource(R.drawable.ic_grade_black_24dp);
                                holder.tv_like_info.setText("Unlike");
                                holder.likeState = true;
                            } else {
                                holder.like_img.setImageResource(R.drawable.ic_stars_black_24dp);
                                holder.tv_like_info.setText("Like");
                                holder.likeState = false;
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();

                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    // if a failure occurs
                    /* Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();*/
                }
            });
        } catch (Exception e) {
            /*    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();*/
        }

        //requete end

        holder.rl_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // it was the 1st button
                Intent myIntent = new Intent(context, ChatActivity.class);
                //myIntent.putExtra("key", value); //Optional parameters
                myIntent.putExtra("Id", modelFeed.getId());
                myIntent.putExtra("content", modelFeed.getContent());
                myIntent.putExtra("likes", modelFeed.getLikes());
                myIntent.putExtra("name", modelFeed.getName());
                myIntent.putExtra("profilpic", modelFeed.getPropic());
                myIntent.putExtra("adminId", modelFeed.getAdminId());
                myIntent.putExtra("username", modelFeed.getName());
                myIntent.putExtra("categorieName", modelFeed.getCategorieName());
                myIntent.putExtra("title", modelFeed.getTitle());
                context.startActivity(myIntent);


            }
        });

        holder.rl_likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //false=liked
                if (!holder.likeState) {
                    holder.like_img.setImageResource(R.drawable.ic_grade_black_24dp);
                    holder.tv_like_info.setText("Unlike");
                    int getCurrenTime = Integer.parseInt(holder.tv_likes.getText().toString()) + 1;
                    holder.tv_likes.setText((getCurrenTime + ""));
                    likeAction(modelFeed.getId());
                    holder.likeState = true;
                } else {
                    holder.like_img.setImageResource(R.drawable.ic_stars_black_24dp);
                    holder.tv_like_info.setText("Like");
                    int getCurrenTime = Integer.parseInt(holder.tv_likes.getText().toString()) - 1;
                    holder.tv_likes.setText(getCurrenTime + "");
                    dislikeAction(modelFeed.getId());
                    holder.likeState = false;
                }


            }
        });

        holder.rl_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Hi. I juste discovered this amazing Holiwo app and his publication." + "\nCheck out the link below and experience it yourself. " + Utils.getServerName() + "holiwo/app/article/" + modelFeed.getId());
                context.startActivity(shareIntent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return modelFeedArrayList.size();
    }

    private void likeAction(int id) {
        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .likeAction("Bearer " + utils.readSharedSetting(context, "token", "token"), id);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void dislikeAction(int id) {
        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .dislikeAction("Bearer " + utils.readSharedSetting(context, "token", "token"), id);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    // if a failure occurs
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public boolean likeState;
        TextView tv_name, tv_time, tv_likes, tv_context;
        RelativeLayout rl_chat;
        RelativeLayout rl_share;
        RelativeLayout rl_likes;
        ImageView imgView_proPic, imgView_postPic;
        RatingBar tv_rate;
        ImageView like_img;
        TextView tv_like_info;

        public MyViewHolder(View itemView) {
            super(itemView);

            imgView_proPic = itemView.findViewById(R.id.imgView_proPic);
            imgView_postPic = itemView.findViewById(R.id.imgView_postPic);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_likes = itemView.findViewById(R.id.tv_like);
            tv_rate = itemView.findViewById(R.id.tv_rate);
            tv_context = itemView.findViewById(R.id.tv_status);
            rl_chat = itemView.findViewById(R.id.rl_chat);
            rl_likes = itemView.findViewById(R.id.rl_likes);
            tv_like_info = itemView.findViewById(R.id.tv_like_info);
            rl_share = itemView.findViewById(R.id.rl_share);
            like_img = itemView.findViewById(R.id.like_img);

        }

    }
}