package tg.master.innov.holiwo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.api_request.ApiRequest;
import tg.master.innov.holiwo.maskView.CercleMaskView;

public class Register2 extends AppCompatActivity {
    private static final int LOGIN_ERROR = 404;
    static String str_mail;
    static String str_password;
    static String str_username;
    TextView loginBtn;
    TextView registerBtn;
    EditText firstname;
    EditText names;
    EditText tels;
    CountryCodePicker towns;
    CercleMaskView propics;
    EditText adress;
    LinearLayout loading_register2;


    View.OnClickListener loginListen = new View.OnClickListener() {
        public void onClick(View v) {
            finish();
        }
    };
    View.OnClickListener registerListen = new View.OnClickListener() {
        public void onClick(View v) {

            init();
            getregister1Information();
            registerConnect();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign2_inter);
        init();
        loginBtn.setOnClickListener(loginListen);
        registerBtn.setOnClickListener(registerListen);
    }

    public void init() {
        loginBtn = findViewById(R.id.loginText);
        registerBtn = findViewById(R.id.signUp);

        firstname = findViewById(R.id.firstName);
        names = findViewById(R.id.name);
        propics = findViewById(R.id.profils_register);
        tels = findViewById(R.id.tel);
        towns = findViewById(R.id.spinner_choose_country);
        adress = findViewById(R.id.address);
        loading_register2 = findViewById(R.id.connecting_register2);


    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }


    @SuppressLint("SetTextI18n")
    public void getregister1Information() {
        //get  register first page Information

        Intent intent = getIntent();
        if (intent != null) {

            if (intent.hasExtra("mail")) {
                str_mail = intent.getStringExtra("mail");
            }
            if (intent.hasExtra("username")) {
                str_username = intent.getStringExtra("username");
            }
            if (intent.hasExtra("password")) {
                str_password = intent.getStringExtra("password");
            }
        }
    }

    public void registerConnect() {
        String mail = str_mail;
        String username = str_username;
        String password = str_password;
        String name = names.getText().toString().trim();
        final String firstName = firstname.getText().toString().trim();
        final String address = adress.getText().toString().trim();
        final String tel = tels.getText().toString().trim();
        /*  String propic =    propics.get().toString().trim();*/
        String town = towns.getSelectedCountryCode();


        loading_register2.setVisibility(View.VISIBLE);
        loginBtn.setVisibility(View.GONE);

        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .register(mail, username, password, name, firstName, address, tel, "", town);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    if (response.code() == 409) {
                        Toast.makeText(getApplicationContext(), "user already exist", Toast.LENGTH_LONG).show();
                    } else if (response.code() == 402) {
                        names.setError("wrong name (must be > 2)");
                    } else if (response.code() == 403) {
                        Toast.makeText(getApplicationContext(), "wrong username (must be length 5 - 12)", Toast.LENGTH_LONG).show();
                    } else if (tel.length() < 8) {
                        tels.setError("tel is not valid");
                    } else if (response.code() == 405) {
                        firstname.setError("wrong firstname  (must be < 3)");
                    } else if (response.code() == 406) {
                        Toast.makeText(getApplicationContext(), "email is not valid", Toast.LENGTH_LONG).show();
                    } else if (response.code() == 407) {
                        Toast.makeText(getApplicationContext(), "password invalid (must length 4 - 8 and include 1 number at least)", Toast.LENGTH_LONG).show();
                    } else if (response.code() == 408) {
                        tels.setError("tel is not valid");
                    } else if (address.isEmpty()) {
                        adress.setError("please this field is importante");
                    } else if (response.code() == 500) {
                        Toast.makeText(getApplicationContext(), "unable to verify user", Toast.LENGTH_LONG).show();
                    } else if (response.code() == 201) {

                        try {
                            //get the response of the request
                            String stringRes = response.body().string();

                            JSONObject res = new JSONObject(stringRes);
//
                            /*Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_LONG).show();*/

                            if (res.get("userId") != null) {
                                // Toast.makeText(getApplicationContext(), res.get("userId").toString(), Toast.LENGTH_LONG).show();
                                Intent it = new Intent(getApplicationContext(), Login.class);
                                startActivity(it);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "user not exist", Toast.LENGTH_LONG).show();
                                loading_register2.setVisibility(View.GONE);
                                loginBtn.setVisibility(View.VISIBLE);

                            }


                        } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        loading_register2.setVisibility(View.GONE);
                        loginBtn.setVisibility(View.VISIBLE);
                    }


                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    // if a failure occurs
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    loading_register2.setVisibility(View.GONE);
                    loginBtn.setVisibility(View.VISIBLE);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), LOGIN_ERROR, Toast.LENGTH_LONG).show();
            loading_register2.setVisibility(View.GONE);
            loginBtn.setVisibility(View.VISIBLE);
        }
        loading_register2.setVisibility(View.GONE);
        loginBtn.setVisibility(View.VISIBLE);
    }

}
