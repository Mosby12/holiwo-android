package tg.master.innov.holiwo.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tg.master.innov.holiwo.MainActivity;
import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.api_request.ApiRequest;
import tg.master.innov.holiwo.utils.Utils;

public class Login extends AppCompatActivity {
    Button registerBtn;
    Button knowMoreBtn;
    Button loginBtn;
    EditText emailFied;
    EditText passwordFied;
    LinearLayout loading;

    Pattern EMAIL_REGEX = Utils.getRegrexMail();
    Pattern PASSWORD_REGEX = Utils.getRegrexPassword();

    String token = null;
    String firstname = null;
    String name = null;
    String town = null;
    String tel = null;
    String propic = null;
    String address = null;
    String username = null;
    String mail2 = null;
    Utils utils = new Utils();

    View.OnClickListener registerListen = new View.OnClickListener() {
        public void onClick(View v) {
            // it was the 1st button
            Intent myIntent = new Intent(Login.this, Register.class);
            //myIntent.putExtra("key", value); //Optional parameters
            Login.this.startActivity(myIntent);
        }
    };
    View.OnClickListener moreListen = new View.OnClickListener() {
        public void onClick(View v) {
            // it was the 2st button

            String url = "http://www.holiwo.africa";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }
    };
    Intent intent;
    boolean isUserFirstTime;

    public void init() {
        registerBtn = findViewById(R.id.signUp);
        knowMoreBtn = findViewById(R.id.knowMore);
        emailFied = findViewById(R.id.email_login);
        passwordFied = findViewById(R.id.password_login);
        loginBtn = findViewById(R.id.loginSubmit);
        loading = findViewById(R.id.connecting);
    }
    View.OnClickListener LoginListen = new View.OnClickListener() {
        public void onClick(View v) {


            if (!EMAIL_REGEX.matcher(emailFied.getText().toString().trim()).matches()) {
                emailFied.setError("Invalid email");
            } else if (!PASSWORD_REGEX.matcher(passwordFied.getText().toString().trim()).matches()) {
                Toast.makeText(getApplicationContext(), "password invalid (must length 4 - 8 and include 1 number at least)", Toast.LENGTH_LONG).show();
            } else {
                init();
                loginConnect();
            }
       /*     // it was the 1st button
            Intent myIntent = new Intent(Login.this, MainActivity.class);
            //myIntent.putExtra("key", value); //Optional parameters
            Login.this.startActivity(myIntent);*/
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_inter);
        readFirstconnectLogin();
    }

    public void loginConnect() {
        String mail = emailFied.getText().toString().trim();
        String password = passwordFied.getText().toString().trim();

        loading.setVisibility(View.VISIBLE);
        loginBtn.setVisibility(View.GONE);

        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .login(mail, password);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    try {
                        //get the response of the request
                        if (response.code() == 500) {
                            Toast.makeText(getApplicationContext(), "unable to verify user", Toast.LENGTH_LONG).show();
                        } else {
                            if (response.code() == 404) {
                                Toast.makeText(getApplicationContext(), "user not exist", Toast.LENGTH_LONG).show();

                            } else {
                                if (response.code() == 403) {
                                    Toast.makeText(getApplicationContext(), "Invalid password", Toast.LENGTH_LONG).show();
                                } else {


                                    String stringRes = response.body().string();

                                    JSONObject res = new JSONObject(stringRes);
//
                                    /*Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_LONG).show();*/

                                    if (res.get("userId") != null) {
                                        // Toast.makeText(getApplicationContext(), res.get("userId").toString(), Toast.LENGTH_LONG).show();

                                        if (res.getString("token") != null) {
                                            token = res.getString("token");
                                            firstname = res.getString("firstname");
                                            name = res.getString("name");
                                            town = res.getString("town");
                                            tel = res.getString("tel");
                                            propic = res.getString("propic");
                                            address = res.getString("address");
                                            username = res.getString("username");
                                            mail2 = res.getString("mail");
                                            Utils.saveSharedSetting(getApplicationContext(), "token", token);
                                            Utils.saveSharedSetting(getApplicationContext(), "firstname", firstname);
                                            Utils.saveSharedSetting(getApplicationContext(), "name", name);
                                            Utils.saveSharedSetting(getApplicationContext(), "town", town);
                                            Utils.saveSharedSetting(getApplicationContext(), "tel", tel);
                                            Utils.saveSharedSetting(getApplicationContext(), "propic", propic);
                                            Utils.saveSharedSetting(getApplicationContext(), "address", address);
                                            Utils.saveSharedSetting(getApplicationContext(), "username", username);
                                            Utils.saveSharedSetting(getApplicationContext(), "mail", mail2);
                                        }
                                        Intent it = new Intent(getApplicationContext(), MainActivity.class);
                                        it.putExtra(Utils.PREF_USER_FIRST_TIME, isUserFirstTime);
                                        utils.saveSharedFirst(Login.this, Utils.PREF_USER_FIRST_TIME, Utils.PREF_USER_FIRST_TIME);
                                        it.putExtra(MainActivity.TOKEN_NAME, token);
                                        startActivity(it);
                                        finish();
                                    } else {
                                        /*   Toast.makeText(getApplicationContext(), "user not exist", Toast.LENGTH_LONG).show();*/
                                        loading.setVisibility(View.GONE);
                                        loginBtn.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        loading.setVisibility(View.GONE);
                        loginBtn.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    // if a failure occurs
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    loading.setVisibility(View.GONE);
                    loginBtn.setVisibility(View.VISIBLE);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.server_no_found, Toast.LENGTH_LONG).show();
            loading.setVisibility(View.GONE);
            loginBtn.setVisibility(View.VISIBLE);
        }
        loading.setVisibility(View.GONE);
        loginBtn.setVisibility(View.VISIBLE);
    }

    public Boolean readFirstconnectLogin() {
        isUserFirstTime = Boolean.valueOf(utils.readSharedFirst(this, Utils.PREF_USER_FIRST_TIME, "true"));

        if (isUserFirstTime) {
            init();
            registerBtn.setOnClickListener(registerListen);
            knowMoreBtn.setOnClickListener(moreListen);
            loginBtn.setOnClickListener(LoginListen);
        } else {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        return isUserFirstTime;
    }

}

