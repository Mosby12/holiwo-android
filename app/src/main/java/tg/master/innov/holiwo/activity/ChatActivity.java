package tg.master.innov.holiwo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.lunainc.chatbar.ViewChatBar;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.adapter.ChatRecyAdapter;
import tg.master.innov.holiwo.maskView.CercleMaskView;
import tg.master.innov.holiwo.model.ChatModel;

public class ChatActivity extends AppCompatActivity {

    SQLiteDatabase database;
    ViewChatBar chatBar;
    private RecyclerView rv;
    private ChatRecyAdapter adapt;
    private static int str_Id = 0;
    private static String str_content = "";
    private static String str_likes = "";
    private static String str_title = "";
    private static String str_categorieName = "";
    private static String str_username = "";
    private static URL str_propic;
    private static String str_name = "";
    private static int str_adminId = 0;

    View.OnClickListener messageSenderListen = new View.OnClickListener() {
        public void onClick(View v) {
            getChatInformation();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                if (!chatBar.getMessageText().isEmpty()) {
                    chatModel = new ChatModel(str_adminId, 1, 3, str_Id, chatBar.getMessageText(), 0);
                    chatMessage.add(chatModel);
                    adapt.notifyDataSetChanged();
                }
            }
            chatBar.setClearMessage(true);
        }
    };

    ChatModel chatModel;
    RequestManager glide;
    private ArrayList<ChatModel> chatMessage = new ArrayList<>();


    View.OnClickListener ChatBarOnclickLister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            rv.scrollToPosition(adapt.getItemCount() - 1);
        }
    };
    private Socket mSocket;


    public ChatActivity() {

    }

    private EditText mInputMessageView;

    {
        try {
            mSocket = IO.socket("http://chat.socket.io");
        } catch (URISyntaxException e) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.disconnect();
        Emitter.Listener onNewMessage = null;
        mSocket.off("new message", onNewMessage);
    }

    private void attemptSend() {
        String message = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            return;
        }

        mInputMessageView.setText("");
        mSocket.emit("new message", message);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatview);
        rv = findViewById(R.id.messages_view);
        rv.setLayoutManager(new LinearLayoutManager(this));

        getChatInformation();

        adapt = new ChatRecyAdapter(this, chatMessage);

        if (chatMessage != null) {
            rv.scrollToPosition(adapt.getItemCount() - 1);
        }

        rv.setAdapter(adapt);

        init();
//        connection();

        chatBar.setSendClickListener(messageSenderListen);
        chatBar.setOnClickListener(ChatBarOnclickLister);
    }

    @SuppressLint("SetTextI18n")
    public void getChatInformation() {
        //get  chat info

        Intent intent = getIntent();
        if (intent != null) {

            if (intent.hasExtra("Id")) {
                str_Id = intent.getIntExtra("Id", 0);
            }
            if (intent.hasExtra("content")) {
                str_content = intent.getStringExtra("content");
            }
            if (intent.hasExtra("likes")) {
                str_likes = intent.getStringExtra("likes");
            }
            if (intent.hasExtra("name")) {
                str_name = intent.getStringExtra("name");
            }
            if (intent.hasExtra("profilpic")) {
                /*   str_propic = intent.getStringExtra("Profilpic");*/
            }
            if (intent.hasExtra("username")) {
                str_username = intent.getStringExtra("username");
            }
            if (intent.hasExtra("categorieName")) {
                str_categorieName = intent.getStringExtra("categorieName");
            }
            if (intent.hasExtra("title")) {
                str_title = intent.getStringExtra("title");
            }
            if (intent.hasExtra("adminId")) {
                str_adminId = intent.getIntExtra("adminId", 0);
            }
            TextView textView_name_info;
            textView_name_info = findViewById(R.id.chattext_nom);
            TextView textView_title_info = findViewById(R.id.chattext_titre);
            CercleMaskView imageView_title_info = findViewById(R.id.chatimage_profile);

            init();
            if (str_propic == null) {
                glide.load(R.drawable.profile).into(imageView_title_info);
            } else {
                glide.load(str_propic).into(imageView_title_info);
            }
            textView_name_info.setText(str_username);
            textView_title_info.setText(str_title);

        }
    }

    public void init() {

        glide = Glide.with(this);
        chatBar = findViewById(R.id.message_sender);
    }

}
