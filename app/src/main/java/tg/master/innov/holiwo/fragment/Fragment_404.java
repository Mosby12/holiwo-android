package tg.master.innov.holiwo.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import tg.master.innov.holiwo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_404 extends Fragment {

    private View view;

    public Fragment_404() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.page_404_no_found, container, false);
        return view;
    }

}
