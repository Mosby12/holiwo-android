package tg.master.innov.holiwo.model;

public class UserModel {

    private int idUser;
    private String userLastname;
    private String userFirstname;
    private String userPro;
    private String userMail;
    private int userType;
    private String userContact;
    private int delete_state;

    public int getIdUser() {
        return idUser;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public String getUserPro() {
        return userPro;
    }

    public String getUserMail() {
        return userMail;
    }

    public int getUserType() {
        return userType;
    }

    public String getUserContact() {
        return userContact;
    }

    public int getDelete_state() {
        return delete_state;
    }
}
