package tg.master.innov.holiwo.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import tg.master.innov.holiwo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNotif extends Fragment {

    TextView tolbar_text;
    View view;

    public FragmentNotif() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notif, container, false);
        init();
        tolbar_text.setText("Notification");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void init() {
        tolbar_text = view.findViewById(R.id.tolbar_text);
    }
}
