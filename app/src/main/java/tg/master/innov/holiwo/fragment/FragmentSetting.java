package tg.master.innov.holiwo.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.activity.Login;
import tg.master.innov.holiwo.api_request.ApiRequest;
import tg.master.innov.holiwo.utils.DialogCustom;
import tg.master.innov.holiwo.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSetting extends Fragment {


    Context context;
    View view;
    Button deconnect_btn;
    LinearLayout whatsappshare;
    LinearLayout facebookshare;
    LinearLayout twittershare;
    TextView deleteAccount;
    View.OnClickListener delete_accListen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogCustom show = new DialogCustom.Builder(context)
                    .setTitle("Do you want delete this account?")
                    .setMessage("If you do this, the operation is not reversion")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(context, "POSSSSSS", Toast.LENGTH_LONG).show();

                            deleteAccount();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .setIcon(R.drawable.ic_sentiment_dissatisfied_black_24dp)
                    .show();
        }
    };
    View.OnClickListener deconnectBtnListen = new View.OnClickListener() {
        public void onClick(View v) {
            deconnect();
        }
    };
    View.OnClickListener facebookbtnListen = new View.OnClickListener() {
        public void onClick(View v) {
            share("com.facebook");
        }
    };
    View.OnClickListener whatsappBtnListen = new View.OnClickListener() {
        public void onClick(View v) {
            share("com.whatsapp");
        }
    };
    View.OnClickListener twitterBtnListen = new View.OnClickListener() {
        public void onClick(View v) {
            share("com.twitter");
        }
    };

    public FragmentSetting(Context context) {
        this.context = context;
    }

    public FragmentSetting() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting, container, false);
        init();
        deconnect_btn.setOnClickListener(deconnectBtnListen);
        whatsappshare.setOnClickListener(whatsappBtnListen);
        twittershare.setOnClickListener(twitterBtnListen);
        facebookshare.setOnClickListener(facebookbtnListen);
        deleteAccount.setOnClickListener(delete_accListen);
        return view;
    }

    public void init() {
        deconnect_btn = view.findViewById(R.id.deconnnect_btn);
        facebookshare = view.findViewById(R.id.facebookshare);
        whatsappshare = view.findViewById(R.id.whatsappshare);
        twittershare = view.findViewById(R.id.twittershare);
        deleteAccount = view.findViewById(R.id.delete_account);
    }

    public void popup_name() {
        // declaration de la dialogue activity
        Dialog mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.dialog_edit_custom);
        EditText edit_profil = (EditText) mDialog.findViewById(R.id.modif_edit);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView title = (TextView) mDialog.findViewById(R.id.modif_title);
        title.setText("NAME");
        mDialog.show();
    }

    public void deconnect() {

        clearSharedPreferences(context);
        Intent intent_deconn = new Intent(getContext(), Login.class);
        startActivity(intent_deconn);
        getActivity().finish();
    }

    public void share(final String packages) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.setPackage(packages);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hi. I juste discovered this amazing Holiwo app and his publication." + "\nCheck out the link below and experience it yourself. " + Utils.getServerName() + "info");
        try {
            context.startActivity(shareIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "this App have not been installed.", Toast.LENGTH_LONG);
        }

    }

    public void clearSharedPreferences(Context ctx) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.deleteSharedPreferences(Utils.PREFERENCES_FILE);
        } else {
            //Get Path of File
            String filePath = ctx.getFilesDir().getParent() + "/shared_prefs/" + Utils.PREFERENCES_FILE + ".xml";
            //Prepare delete the files
            File deletePrefFile = new File(filePath);
            //Commit delete the files
            deletePrefFile.delete();
            //Make sure it has enough time to save all the commited changes
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

        }
    }

    public void deleteAccount() {
        Utils u = new Utils();
        try {
            Call<ResponseBody> call = ApiRequest
                    .getInstance()
                    .getApi()
                    .deleteAccompte(u.readSharedSetting(context, "token", "token"), 1);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    try {
                        if (response.code() == 201) {
                            deconnect();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        //        swipeContainer.setRefreshing(false);
                        Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    // if a failure occurs

                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {

            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
