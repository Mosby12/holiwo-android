package tg.master.innov.holiwo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import tg.master.innov.holiwo.fragment.FragmentAbout;
import tg.master.innov.holiwo.fragment.FragmentChatList;
import tg.master.innov.holiwo.fragment.FragmentEditProfil;
import tg.master.innov.holiwo.fragment.FragmentNotif;
import tg.master.innov.holiwo.fragment.FragmentProfile;
import tg.master.innov.holiwo.fragment.FragmentPublish;
import tg.master.innov.holiwo.fragment.FragmentSetting;

public class MainActivity extends AppCompatActivity {

    public static final String TOKEN_NAME = "";
    FragmentTransaction fragmentTransaction;
    FragmentProfile fragmentProfile;
    FragmentNotif fragmentNotif;
    FragmentChatList fragmentChatList;
    FragmentPublish fragmentPublish;
    FragmentSetting fragmentSetting;
    FragmentEditProfil fragmentEditProfil;
    FragmentAbout fragmentAbout;
    DrawerLayout dl;
    NavigationView nv;
    FloatingActionButton fab;
    BottomAppBar bar;
    static int code;

    View profils;
    View app_bar_chat;
    View app_bar_notif;


    View.OnClickListener barClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            dl.openDrawer(Gravity.LEFT);
        }
    };

    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        FragmentTransaction replace = fragmentTransaction.replace(R.id.publi_zone, fragmentPublish);
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        nv.setItemIconTintList(null);

        fab.setEnabled(false);
        setSupportActionBar(bar);
        bar.setNavigationOnClickListener(barClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.bottom_bar_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        init();


        switch (item.getItemId()) {
            case R.id.app_bar_notif:
                fragmentTransaction.replace(R.id.publi_zone, fragmentNotif);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                app_bar_notif.setEnabled(false);
                fab.setEnabled(true);
                profils.setEnabled(true);
                app_bar_chat.setEnabled(true);

                return true;

            case R.id.app_bar_chat:

                fragmentTransaction.replace(R.id.publi_zone, fragmentChatList);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                fab.setEnabled(true);
                profils.setEnabled(true);
                app_bar_notif.setEnabled(true);
                app_bar_chat.setEnabled(false);
                return true;

            case R.id.profils:

                fragmentTransaction.replace(R.id.publi_zone, fragmentProfile);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                fab.setEnabled(true);
                profils.setEnabled(false);
                app_bar_chat.setEnabled(true);
                app_bar_notif.setEnabled(true);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void init() {

        nv = findViewById(R.id.nv);
        dl = findViewById(R.id.dl);
        profils = findViewById(R.id.profils);
        app_bar_chat = findViewById(R.id.app_bar_chat);
        app_bar_notif = findViewById(R.id.app_bar_notif);

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fab = findViewById(R.id.fab);
        bar = findViewById(R.id.bar);
        fragmentProfile = new FragmentProfile(this);
        fragmentEditProfil = new FragmentEditProfil(this);
        fragmentNotif = new FragmentNotif();
        fragmentChatList = new FragmentChatList(this);
        fragmentAbout = new FragmentAbout();
        fragmentPublish = new FragmentPublish(this);
        fragmentSetting = new FragmentSetting(this);


        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                dl.closeDrawer(Gravity.LEFT);
                return drawerItemtSelected(id);
            }
        });
    }


    public boolean drawerItemtSelected(int id) {
        init();

        switch (id) {
            case R.id.about_item:
                // it was the 1st button
                fragmentTransaction.replace(R.id.publi_zone, fragmentAbout);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                fab.setEnabled(true);
                profils.setEnabled(true);
                app_bar_chat.setEnabled(true);
                app_bar_notif.setEnabled(true);

                break;
            case R.id.help_and_comments_item:

                String url = "http://www.holiwo.africa";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                fab.setEnabled(true);
                profils.setEnabled(true);
                app_bar_chat.setEnabled(true);
                app_bar_notif.setEnabled(true);

                break;
            case R.id.setting_item:

                fragmentTransaction.replace(R.id.publi_zone, fragmentSetting);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                fab.setEnabled(true);
                profils.setEnabled(true);
                app_bar_chat.setEnabled(true);
                app_bar_notif.setEnabled(true);

                break;
            case R.id.share_item:
                fragmentTransaction.replace(R.id.publi_zone, fragmentEditProfil);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                fab.setEnabled(true);
                profils.setEnabled(true);
                app_bar_chat.setEnabled(true);
                app_bar_notif.setEnabled(true);
                break;
        }
        return false;
    }

    public void Home_btn(View view) {
        /*Toast.makeText(this, "Home btn", Toast.LENGTH_SHORT).show();*/

        /*    this.getSupportFragmentManager().beginTransaction().remove(fragmentNotif).commit();*/
        init();
        FragmentTransaction replace;
        replace = fragmentTransaction.replace(R.id.publi_zone, fragmentPublish);
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        fab.setEnabled(false);
        View profils = findViewById(R.id.profils);
        profils.setEnabled(true);
        View app_bar_chat = findViewById(R.id.app_bar_chat);
        app_bar_chat.setEnabled(true);
        View app_bar_notif = findViewById(R.id.app_bar_notif);
        app_bar_notif.setEnabled(true);
    }


}
