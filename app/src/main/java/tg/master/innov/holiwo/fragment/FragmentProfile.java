package tg.master.innov.holiwo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.hbb20.CountryCodePicker;

import java.net.MalformedURLException;
import java.net.URL;

import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.maskView.CercleMaskView;
import tg.master.innov.holiwo.utils.Utils;


public class FragmentProfile extends Fragment {

    View view;
    Context context;
    CercleMaskView user_propic;
    TextView user_Username;
    TextView user_name;
    TextView user_fisrtname;
    TextView user_address;
    TextView user_tel;
    TextView user_follow;
    TextView user_mail;
    CountryCodePicker user_contry;
    RequestManager glide;


    Utils utils = new Utils();

    public FragmentProfile() {
        // Required empty public constructor
    }

    public FragmentProfile(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        init();
        getUserProfile();
        return view;
    }

    public void getUserProfile() {
        URL default_url = null;
        URL propic_url = null;
        try {
            default_url = new URL("http://");
            propic_url = new URL(utils.readSharedSetting(context, "propic", "propic"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            if (propic_url.equals(default_url)) {
                glide.load(R.drawable.profile).error(R.drawable.profile).into(user_propic);
            } else {
                glide.load(propic_url).error(R.drawable.profile).placeholder(R.drawable.profile).fallback(R.drawable.profile).into(user_propic);
            }
        } catch (Exception e) {
            glide.load(R.drawable.profile).error(R.drawable.profile).into(user_propic);
        }


        user_name.setText(utils.readSharedSetting(context, "name", "name"));
        user_Username.setText(utils.readSharedSetting(context, "username", "username"));
        user_fisrtname.setText(utils.readSharedSetting(context, "firstname", "firstname"));
        user_address.setText(utils.readSharedSetting(context, "address", "address"));
        user_tel.setText(utils.readSharedSetting(context, "tel", "tel"));
        user_contry.setCountryForNameCode(utils.readSharedSetting(context, "town", "town"));
        user_mail.setText(utils.readSharedSetting(context, "mail", "mail"));
        user_contry.setEnabled(false);
    }


       /* public void getUserProfile() {


        try {
            Call<ResponseBody> call = UserRequest
                    .getInstance()
                    .getApi()
                    .getUserProfile("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzZXIiOjEsInN0YXRlIjowLCJpYXQiOjE1NTgwODg3NDEsImV4cCI6MTU1ODA5MjM0MX0.oJUHzveO5xnvkSbx_dDPxlOBI074ungKM_DcBf-VK6c");

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                    try {


                        String stringRes = response.body().string();
                        JSONObject  res = new JSONObject(stringRes);
                        if (res.get("id") != null) {
                            user_Username.setText(res.getString("username"));

                            URL default_url = null;
                            URL propic_url = null;
                            try {
                                default_url=    new URL("http://");
                                propic_url  =new URL(res.getString("propic"));
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }

                            if (propic_url.equals(default_url)){
                                glide.load(R.drawable.profile).into(user_propic);
                            }else{
                                glide.load(propic_url).into(user_propic);
                            }
                                    user_name.setText(res.getString("name"));
                            user_fisrtname.setText(res.getString("firstname"));
                                    user_address.setText(res.getString("address"));
                            user_tel.setText(res.getString("tel"));
                            user_contry.setCountryForNameCode(res.getString("town"));


                     *//*       if (res.getInt("")<=1){
                                    user_follow.setText(""+"follow");}else {
                                user_follow.setText(""+"follows");
                            }*//*

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                    *//*Toast.makeText(getApplicationContext(),response.code(),Toast.LENGTH_LONG).show();*//*


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }catch (Exception ignored){
            Toast.makeText(context, ignored.getMessage(), Toast.LENGTH_LONG).show();
        }
    }*/

    public void init() {
        glide = Glide.with(context);
        user_Username = view.findViewById(R.id.user_Username);
        user_propic = view.findViewById(R.id.user_propic);
        user_name = view.findViewById(R.id.user_name);
        user_fisrtname = view.findViewById(R.id.user_firstname);
        user_address = view.findViewById(R.id.user_adrress);
        user_tel = view.findViewById(R.id.user_tel);
        user_follow = view.findViewById(R.id.user_follow);
        user_mail = view.findViewById(R.id.user_email);
        user_contry = view.findViewById(R.id.user_country);

    }

}
