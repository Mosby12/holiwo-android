package tg.master.innov.holiwo.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import tg.master.innov.holiwo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChatList extends Fragment {

    private Context context;

    public FragmentChatList() {
        // Required empty public constructor
    }

    public FragmentChatList(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_chat_list, container, false);

        RecyclerView rv = view.findViewById(R.id.listeDiscut);
        rv.setLayoutManager(new LinearLayoutManager(context));


        return view;
    }

}
