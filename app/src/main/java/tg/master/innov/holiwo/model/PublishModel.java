package tg.master.innov.holiwo.model;


import java.net.URL;

public class PublishModel {
    int id, likes, AdminId, CategorieId, CurrencyId, state;

    String name, categorieName, time, content, title, createdAt, updatedAt, symbol;
    float price, rate;
    URL propic, attachment, attachment1, attachment2;

    public PublishModel() {
    }

    public PublishModel(int id, int likes, int adminId, int categorieId, int currencyId, int state, String name, String categorieName, String time, String content, String title, String createdAt, String updatedAt, String symbol, float price, float rate, URL propic, URL attachment, URL attachment1, URL attachment2) {
        this.id = id;
        this.likes = likes;
        AdminId = adminId;
        CategorieId = categorieId;
        CurrencyId = currencyId;
        this.state = state;
        this.name = name;
        this.categorieName = categorieName;
        this.time = time;
        this.content = content;
        this.title = title;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.symbol = symbol;
        this.price = price;
        this.rate = rate;
        this.propic = propic;
        this.attachment = attachment;
        this.attachment1 = attachment1;
        this.attachment2 = attachment2;
    }

    public String getCategorieName() {
        return categorieName;
    }

    public void setCategorieName(String categorieName) {
        this.categorieName = categorieName;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getAdminId() {
        return AdminId;
    }

    public void setAdminId(int adminId) {
        AdminId = adminId;
    }

    public int getCategorieId() {
        return CategorieId;
    }

    public void setCategorieId(int categorieId) {
        CategorieId = categorieId;
    }

    public int getCurrencyId() {
        return CurrencyId;
    }

    public void setCurrencyId(int currencyId) {
        CurrencyId = currencyId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public URL getPropic() {
        return propic;
    }

    public void setPropic(URL propic) {
        this.propic = propic;
    }

    public URL getAttachment() {
        return attachment;
    }

    public void setAttachment(URL attachment) {
        this.attachment = attachment;
    }

    public URL getAttachment1() {
        return attachment1;
    }

    public void setAttachment1(URL attachment2) {
        this.attachment2 = attachment2;
    }

    public URL getAttachment2() {
        return attachment2;
    }

    public void setAttachment2(URL attachment2) {
        this.attachment2 = attachment2;
    }
}