package tg.master.innov.holiwo.fragment;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.RequestManager;
import com.hbb20.CountryCodePicker;

import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.maskView.CercleMaskView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentEditProfil extends Fragment {

    final static int SELECT_PICTURE = 1;
    Context context;
    View view;
    CercleMaskView profilePicture;
    LinearLayout llUsername;
    LinearLayout llAddress;
    LinearLayout llTel;
    EditText user_Username2;
    EditText user_name2;
    EditText user_fisrtname2;
    EditText user_address2;
    EditText user_tel2;
    EditText user_follow2;
    EditText user_mail2;
    CountryCodePicker user_contry2;
    RequestManager glide2;

    public FragmentEditProfil() {
        // Required empty public constructor
    }

    public FragmentEditProfil(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edit_profil_layout, container, false);
        init();


        return view;

    }

    private void init() {
        profilePicture = view.findViewById(R.id.profilePicture);
        listner();
    }

    private void listner() {

        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Création puis ouverture de la boite de dialogue
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
            }
        });

    }

    /**
     * Retour de la boite de dialogue
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SELECT_PICTURE:
                    String path = getRealPathFromURI(data.getData());
                    Log.d("Choose Picture", path);
                    //Transformer la photo en Bitmap
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    //Afficher le Bitmap
                    profilePicture.setImageBitmap(bitmap);
                    //Renseigner les informations status
          /*          textView.setText("");
                    textView.append("Fichier: " + path);
                    textView.append(System.getProperty("line.separator"));
                    textView.append("Taille: " + bitmap.getWidth() + "px X " + bitmap.getHeight() + " px");*/
                    break;
            }
        }
    }

    /**
     * Obtenir le chemin vers une ressource
     *
     * @param contentURI
     * @return
     */
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


}
