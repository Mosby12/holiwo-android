package tg.master.innov.holiwo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.model.ChatModel;

public class ChatRecyAdapter extends RecyclerView.Adapter<ChatRecyAdapter.MyViewHolder> {


    public static int ME = 1, HIM = 2;
    private static List<ChatModel> listChat;
    Context context;
    private int a;

    public ChatRecyAdapter(Context context, List<ChatModel> listChat) {
        ChatRecyAdapter.listChat = listChat;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        int listSize;
        if (listChat == null) {
            listSize = 0;
        } else {
            listSize = listChat.size();
        }

        return listSize;
    }


    // ordre dexcecution 1
    @Override
    public int getItemViewType(int position) {
        a = listChat.get(position).getType_sender();
        System.out.println("la valeur de a est " + a + "  et position est " + position);
        return a;
    }


    // 2
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // CREATE VIEW HOLDER AND INFLATING ITS XML LAYOUT
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = null;
        if (viewType == ME) {
            view = inflater.inflate(R.layout.my_chat_model, parent, false);
        } else {
            view = inflater.inflate(R.layout.their_chat_model, parent, false);
        }


        return new MyViewHolder(view);
    }


    // 3
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.display(listChat.get(position));
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name = itemView.findViewById(R.id.message_body);


        public MyViewHolder(final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

        public void display(ChatModel listMessage) {


            name.setText(listMessage.getContent_text());


        }

    }
}
