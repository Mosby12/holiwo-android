package tg.master.innov.holiwo.model;

public class ChatModel {
    private int id;
    private int AdminId;
    //type_sender: 1=user ,2 = publisher
    private int type_sender;
    private int UserId;
    private int PublicationId;
    private String content_text;
    private String attachment;
    private int state;
    private String createdAt;
    private int updatedAt;

    public ChatModel() {
    }

    public ChatModel(int adminId, int type_sender, int userId, int publicationId, String content_text, int state) {
        this.id = id;
        AdminId = adminId;
        this.type_sender = type_sender;
        UserId = userId;
        PublicationId = publicationId;
        this.content_text = content_text;
        this.attachment = attachment;
        this.state = state;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdminId() {
        return AdminId;
    }

    public void setAdminId(int adminId) {
        AdminId = adminId;
    }

    public int getType_sender() {
        return type_sender;
    }

    public void setType_sender(int type_sender) {
        this.type_sender = type_sender;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getPublicationId() {
        return PublicationId;
    }

    public void setPublicationId(int publicationId) {
        PublicationId = publicationId;
    }

    public String getContent_text() {
        return content_text;
    }

    public void setContent_text(String content_text) {
        this.content_text = content_text;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(int updatedAt) {
        this.updatedAt = updatedAt;
    }
}