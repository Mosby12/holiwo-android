package tg.master.innov.holiwo.model;

public class NotifModel {

    private int notifImageDrawable;
    private String notifTitle;
    private String notifContent;
    private String notifUrl;
    //send =1 , receive=2, read=3, delete=3;
    private int notifDelete_state;

    public int getNotifImageDrawable() {
        return notifImageDrawable;
    }

    public String getNotifTitle() {
        return notifTitle;
    }

    public String getNotifContent() {
        return notifContent;
    }

    public String getNotifUrl() {
        return notifUrl;
    }

    public int getNotifDelete_state() {
        return notifDelete_state;
    }

    public void setNotifDelete_state(int notifDelete_state) {
        this.notifDelete_state = notifDelete_state;
    }
}
