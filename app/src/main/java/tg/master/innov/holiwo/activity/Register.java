package tg.master.innov.holiwo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Pattern;

import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.utils.Utils;

public class Register extends AppCompatActivity {

    TextView loginBtn;
    TextView nextBtn;
    EditText mail;
    EditText password;
    EditText username;
    LinearLayout loading_register;

    Pattern EMAIL_REGEX = Utils.getRegrexMail();
    Pattern PASSWORD_REGEX = Utils.getRegrexPassword();

    View.OnClickListener loginListen = new View.OnClickListener() {
        public void onClick(View v) {
            finish();
        }
    };
    View.OnClickListener nextListen = new View.OnClickListener() {
        public void onClick(View v) {
            init();
            // it was the 1st button
            loading_register.setVisibility(View.VISIBLE);
            nextBtn.setVisibility(View.GONE);
            if (username.getText().toString().trim().length() >= 13 || username.getText().toString().trim().length() <= 4) {
                username.setError("wrong username (must be length 5 - 12)");
            } else if (!EMAIL_REGEX.matcher(mail.getText().toString().trim()).matches()) {
                mail.setError("Invalid email");
            } else if (!PASSWORD_REGEX.matcher(password.getText().toString().trim()).matches()) {
                Toast.makeText(getApplicationContext(), "password invalid (must length 4 - 8 and include 1 number at least)", Toast.LENGTH_LONG).show();
            } else {
                Intent myIntent = new Intent(Register.this, Register2.class);
                //myIntent.putExtra("key", value); //Optional parameters
                myIntent.putExtra("mail", mail.getText().toString().trim());
                myIntent.putExtra("username", username.getText().toString().trim());
                myIntent.putExtra("password", password.getText().toString().trim());
                Register.this.startActivity(myIntent);
                /*      finish();*/
            }
            loading_register.setVisibility(View.GONE);
            nextBtn.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_inter);
        init();
        loginBtn.setOnClickListener(loginListen);
        nextBtn.setOnClickListener(nextListen);
    }

    public void init() {
        loginBtn = findViewById(R.id.loginText);
        nextBtn = findViewById(R.id.next);
        loading_register = findViewById(R.id.connecting_register);
        mail = findViewById(R.id.email_register);
        username = findViewById(R.id.username_register);
        password = findViewById(R.id.password_register);
    }

    public void ctrl_field() {

    }
}
