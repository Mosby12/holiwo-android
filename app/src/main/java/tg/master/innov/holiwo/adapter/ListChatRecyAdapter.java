package tg.master.innov.holiwo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tg.master.innov.holiwo.R;
import tg.master.innov.holiwo.activity.ChatActivity;
import tg.master.innov.holiwo.model.ChatModel;

public class ListChatRecyAdapter extends RecyclerView.Adapter<ListChatRecyAdapter.MyViewHolder> {

    private final Context context;
    private List<ChatModel> listPublication;
    private String title_publication;

    public ListChatRecyAdapter(Context context, List<ChatModel> listPublication) {

        this.listPublication = listPublication;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return this.listPublication.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // CREATE VIEW HOLDER AND INFLATING ITS XML LAYOUT
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_chat_model, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        holder.display(this.listPublication.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // it was the 1st button
                Intent myIntent = new Intent(context, ChatActivity.class);
                ChatModel chatModel = new ChatModel();
                //myIntent.putExtra("key", value); //Optional parameters
                myIntent.putExtra("Id", chatModel.getId());
                myIntent.putExtra("content", chatModel.getContent_text());
                myIntent.putExtra("name", chatModel.getAdminId());
                myIntent.putExtra("Profilpic", chatModel.getAdminId());

                context.startActivity(myIntent);
            }
        });

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name = itemView.findViewById(R.id.text_Admin_Username);
        private final TextView Title = itemView.findViewById(R.id.text_titre);
        private final ImageView Image_profile = itemView.findViewById(R.id.Image_profile);

        public MyViewHolder(final View itemView) {
            super(itemView);
        }

        public void display(ChatModel a) {

            name.setText("jojo");
            Title.setText("hahah");

        }
    }
}
